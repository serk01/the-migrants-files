$(window).load(function ($) {



    if (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
        skrollr.init({
            forceHeight: false
        });

    } else {
        //do some stuff if we are on mobile
        console.log("on mobile");

        d3.select('nav')
            .style("opacity",'1')
            .style('margin-top', '0px');

        d3.select('#header-text-container')
            .style('top', '20px');


    }
    loadData();

});

$(function () {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: 'Επιστροφή στην κορυφή', // Text for element
        activeOverlay: false// Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });
});

var $root = $('html, body');
$('a').click(function() {
    var hash = $.attr(this, 'href');
    $root.animate({
        scrollTop: $(hash).offset().top
    }, 500, function () {
        window.location.hash = hash;
    });
    return false;
});

//load the data after the page has finished loading
var incomingData;
var dataArray = [];
var flatArray = [];
var maxDeadAndMissing = 0;

var showClickHelperText = true;
var selectedNode;

var markerColor = {
    "Drowned": "#809FFF",
    "Frozen": "#BF008F",
    "Minefield": "#905D00",
    "Car accident": "#999966",
    "Shot": "#9D54C4",
    "Unknown": "#FFFFFF",
    "Suffocated": "#336633",
    "Suicide - other": "#808080"
};

var athens = [37.9667, 23.7167];

var map = L.map('map', {
    center: athens,
    zoom: 7,
    minZoom: 7,
    maxZoom: 9,
    dragging: false,
    scrollWheelZoom: false,
    doubleClickZoom: false,
    zoomControl: false,
    attributionControl: false
});

var isTimelapseFinished = false;

var MODE = {
    "Normal": 0,
    "Timelapse": 1
};

var mode = MODE.Normal;

//add layer from mapbox using the leaflet-providers plugin
L.tileLayer.provider('MapBox.serko.TheMigrantsFiles').addTo(map);


//add attribution
L.control.attribution({position: "bottomleft"}).addTo(map);

//create a new Sidebar
var sidebar = L.control.sidebar('sidebar', {
    position: 'right',
    autoPan: false
});

var sidebarContainer = sidebar.getContainer();

//add the sidebar to the map
map.addControl(sidebar);

function exists (jsonObject, key) {
    return jsonObject[key] != undefined;
}

var aggregatedEvents = {
};

/*
 Parses the incoming data
 Then it creates an array of events that are aggregated based on their lat/long, computing
 the total victims for each location
 */
function parseData (d) {
    var lat = d.lat,
        lng = d.lng,
        latlng = L.latLng(lat, lng),    //create a latLng object
        event = {}; //create an empty object

    event.data = d;
    event.latlng = latlng;

//        if an event with this latlng doesn't already exist
    if ( !exists(aggregatedEvents, latlng) ) { //noinspection JSHint

//        create a new array, add this event in it and use the latlng as the key of the JSON object
        aggregatedEvents[latlng] = [event];

//        create a new json object that will store the sum of all the related events' victims value
        aggregatedEvents[latlng].total = event.data.Dead_and_missing;

//        create bounds
        var southWest = L.latLng(lat, lng),
            northEast = L.latLng(lat, lng);

//        set the bounds
        aggregatedEvents[latlng].bounds = L.latLngBounds(southWest, northEast);


    } else { //if an event with this latlng already exists

        //get the existing array using the latlng as the key
        var eventsArray = aggregatedEvents[latlng];

        //add the new event object to the JSON Array
        eventsArray.push(event);

        //get the existing total
        var total = eventsArray.total;

//        add to it the new event's dead and missing value
        total += event.data.Dead_and_missing;
        eventsArray.total = total;
    }
}

function cleanSidebar () {
    //clean previous data
    sidebarContainer.innerHTML = "";
}

function populateSidebar (data) {

    var total = data.total;

//    TODO: get maxyear and minyear and don't hard code
    var summary = "<p class='summary-total'>" + total + " νεκροί/αγνοούμενοι στην περιοχή</br><span class='summary-year'>την περίοδο 2000-2013</span></p>";
    var analitika =
        "<div class='sidebar-content'>" +
            "<img src='images/moreDetails_icon.png' style='float: left;'/>" +
            "<p class='analitika-title sidebar-details-content'>Αναλυτικά</p>" +
            "</div><hr class='horizontal-divider'>";


    $('#sidebar').append(summary);
    $('#sidebar').append(analitika);

    //append each event's information to the sidebar
    for ( var i = 0; i < data.length; i++ ) {

        var selectedEvent = data[i];
        var d = selectedEvent.data;

        var title = "<div class='details-title sidebar-content'>" +
            "<img src='images/Place_icon.png' style='float: left;'/>" +
            "<p class='sidebar-details-content'>" + d.Location + ". " + d.Dead_and_missing + " νεκροί/αγνοούμενοι</p>";

        var date = "<div class='details-date sidebar-content'>" +
            "<img src='images/Date_icon.png' style='float: left;'/>" +
            "<p class='sidebar-details-content'>" + getFormattedDate(d.Date) + "</p>";

        var desc = "<div class='details-desc sidebar-content'>" +
            "<img src='images/description_icon.png' style='float: left;'/>" +
            "<p class='sidebar-details-content'>" + d.Description_Greek + "</p>";

        $('#sidebar').append(title);
        $('#sidebar').append(date);
        $('#sidebar').append(desc);

        if ( i < data.length - 1 ) {
            $('#sidebar').append("<hr class='horizontal-divider'>");
        }

        sidebar.show();
        $('#sidebar').scrollTop();
    }

}

function getDataAsArrayOfArrays () {
    var eventsArray = [];

    for ( var event in aggregatedEvents ) {
        eventsArray.push(aggregatedEvents[event]);
    }
    return eventsArray;
}

function highlightMarker (node) {
    node
        .transition()
        .duration(350)
        .style("fill-opacity", 1);
}

function unHighlightMarker (node) {
    node
        .transition()
        .duration(350)
        .style("fill-opacity", 0.6);
}

function resetMapForNewSelection () {
    map.closePopup();

    if ( selectedNode != undefined ) {
        unHighlightMarker(selectedNode);
    }

    d3.selectAll('#piechart')
        .transition(2000)
        .style('opacity', 0)
        .remove();
}

//called after a circle has been animated in
function attachInteractionTriggers () {

    var popup;
    //event listeners for each circle
    d3.selectAll('circle')
        .on("mouseover", function (d) {
            popup = showPopup(d);
            highlightMarker(d3.select(this));
        })
        .on("mouseout", function () {
            map.closePopup();

            //if we have a marker selected (by selected we mean clicked and zoomed-in on it,
            //then mouseout should not unhighlight it. so here we check that scenario
            if ( selectedNode != undefined ) {
                var thisCx = d3.select(this).attr("cx");
                var thisCy = d3.select(this).attr("cy");
                var selectedCx = selectedNode.attr('cx');
                var selectedCy = selectedNode.attr('cy');

                if ( selectedCx != thisCx || selectedCy != thisCy ) {
                    unHighlightMarker(d3.select(this));
                }
            } else { //if we don't have a selected Node then unhighlight works for all markers
                unHighlightMarker(d3.select(this));
            }
        })
        .on("click", function (d) {
            resetMapForNewSelection();
            selectedNode = d3.select(this);
            showClickHelperText = false;
            hideTimelapseButton();

            d3.select(this)
                .transition()
                .duration(350)
                .style("fill-opacity", 1);
            //            call stopPropagation so we prevent the onclick event from propagating up to the #map object.
            //            the map has it's own onclick event which shouldn't be called in this context
            d3.event.stopPropagation();

            if ( map.getZoom() === map.getMaxZoom() ) {
                map.panTo(getNewLocationWithOffset(d.bounds, false));
            }
            else {
                map.fitBounds(getNewLocationWithOffset(d.bounds, true));
            }

            map.addOneTimeEventListener("moveend", presentData);

            function presentData () {
                cleanSidebar();
                populateSidebar(d);
                toGraphOrNotToGraph(d);
                drawPieChart(d);
                connectToSidebar(d);
            }
        });


    //this event needs to be attached to the map object because the map distinguishes click from move events
    //if we used d3 to attach this event paning the map and then lifting the mouse finger would be interpreted
    //as a click and would trigger it eventhough it was a pan not a click (in the map context)
    map.on("click", function () {
        resetMapForNewSelection();
//        unHighlightMarker(selectedNode);
        //reset selected node
        selectedNode = null;

        if ( map.getZoom() === map.getMaxZoom() ) {
            map.zoomOut(map.getMinZoom());

            map.addOneTimeEventListener("moveend", function () {
                map.panTo(athens);
                if ( sidebar.isVisible() ) {
                    sidebar.hide();
                }
            });
            showTimelapseButton();
            clearConnectors();
        }
    });

    map.on('move', clearConnectors);


//    map.on('zoomstart', function() {
//        d3.selectAll('circle')
//            .style('opacity',0);
//    })
//
//    map.on('zoomend', function() {
//        d3.selectAll('circle')
//            .style('opacity',1);
//    })

}


function drawPieChart (data) {
    var array = [];
    var dict = {};

    data.forEach(function (d) {
        var key = d.data.Cause_of_death;
        if ( key.length === 0 ) {
            key = "Unknown";
        }
        if ( !exists(dict, key) ) {
            dict[key] = {}; //create json object
            array.push(dict[key]); //push to an array for d3 manipulation
            dict[key].cause = key;
            dict[key].causegr = d.data.Cause_of_death_Greek
            dict[key].victims = d.data.Dead_and_missing;
        } else {
            dict[key].victims += d.data.Dead_and_missing;
        }
    });

    var width = 460,
        height = 300;

    var scaleSize = d3.scale.linear().domain([0, getMaxVictims()]).range([10, 100]);

    var pie = d3.layout.pie()
        .sort(null)
        .value(function (d) {
            return scaleSize(d.victims);
//            return d.victims;
        });

    var radius = Math.round(selectedNode.attr('r')) + 10;

    var arc = d3.svg.arc()
        .innerRadius(radius)
        .outerRadius(radius + 30);

    var marker = data[0];

    var svg = d3.select("g")
        .append('svg')
        .attr('id', 'piechart')
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + selectedNode.attr('cx') + "," + selectedNode.attr('cy') + ")");

    var path = svg.selectAll("path")
        .data(pie(array))
        .enter().append("path")
        .attr('class', 'donut')
        .style("fill", function (d, i) {
            return markerColor[d.data.cause];
        })
        .attr("d", arc)
        .on('mouseover', function (d) {
            console.log("mouseover");
            d3.select(this)
                .style('fill-opacity', 1);
            popup = showPieChartPopup(d, marker);
        })
        .on('mouseout', function () {
            console.log("mouseout");
            d3.select(this)
                .style('fill-opacity', 0.4);

            map.closePopup();
        })
        .transition()
        .duration(1000)
        .styleTween('opacity', function () {
            return d3.interpolate('0', '1');
        });


}

function connectToSidebar (d) {
    var sidebar = d3.select('#sidebar');
    var marker = d[0];
    var point = map.latLngToLayerPoint(marker.latlng);

    d3.select('g').insert('line', ':first-child')
        .style('stroke', '#F9BA6A')
        .attr('x1', point.x)
        .attr('y1', point.y)
        .attr('x2', function () {
            return point.x;
        })
        .attr('y2', function () {
            return point.y;
        })
        .transition()
        .duration(800)
        .attr('x2', function () {
            return point.x + 1000;
        })
        .attr('y2', function () {
            return point.y;
        });
}

function clearConnectors () {
    d3.selectAll('line').remove();
}

//this is used so that markers are in the center o the screen after the sidebar is opened.Other approaches
//proved to be either less smooth or less consistent. There must be a better way to do this, but this works for now
function getNewLocationWithOffset (bounds, returnBounds) {
    var returnV;
    var centerLatLng = bounds.getCenter();
    var centerPoint = map.latLngToLayerPoint(centerLatLng);

    if ( returnBounds ) {     //create new bounds
        centerPoint.x += 50; //add the x offset we want
        var newCenterlatlng = map.layerPointToLatLng(centerPoint);
        var southWest, northEast, offsetBounds;
        southWest = newCenterlatlng;
        northEast = newCenterlatlng;

        returnV = L.latLngBounds(southWest, northEast);

    } else { //create new latlng
        centerPoint.x += 200; //add the x offset we want. Different offset needed since we return a different object
        returnV = map.layerPointToLatLng(centerPoint);
    }

    return returnV;
}

function showPieChartPopup (d, marker) {
    var cause = d.data.causegr;
    var victims = d.data.victims;
    var content = cause + ":" + victims;

    var popup = L.popup()
        .setLatLng(marker.latlng)
        .setContent(content);
    popup.openOn(map);

//    When hovering over the pie chart occasionaly the mouse pointer hovers over the popup tooltip which results
//    in mouseout events getting called on the pie chart which in turn result to flikering as the opacity changes
//    by specifiying pointer-events:none on the tooltip we make sure it never becomes the target of mouse events
//    which fixes the flicerking problem

    d3.select('.leaflet-popup-content-wrapper')
        .style('pointer-events', 'none');


    return popup;
}

function showPopup (d) {
    var victims_greek = d.total === 1 ? "θύμα" : "θύματα";
    var content = d.total + " " + victims_greek + " σε αυτή την περιοχή";
    if ( showClickHelperText ) {
        content += "<br> Κάντε click για λεπτομέρειες";
    }

    var popup = L.popup()
        .setLatLng(getNodeDataFromDataset(d).latlng)
        .setContent(content);
    popup.openOn(map);
    return popup;
}

function toGraphOrNotToGraph (data) {
    var returnV = false;
    var parseDate = d3.time.format("%Y").parse;
    //if we have less than one events in this area, then it doesn't make sense to plot it
    //however we might have more than one events and still be in the same year so we need
    //to check that as well later on
    if ( data.length > 1 ) {

        var yearlyTotalsArray = [];
        var yearsData = {};
        var counter = 0;
        //create JSON representations of yearly data
        data.forEach(function (d) {
            var key = String(d.data.Year);
            if ( !exists(yearsData, key) ) {
                yearsData[key] = {}; //create empty object
                yearsData[key].date = parseDate(String(d.data.Year));
                yearsData[key].victims = d.data.Dead_and_missing;
                counter += 1; //increment for every new object
            } else {
                yearsData[key].victims += d.data.Dead_and_missing;
            }
        });

        //if the counter is <= 1 then we only have data for one year, which doesn't make sense to plot
        if ( counter > 1 ) {
            //convert objects to an array of objects
            for ( var object in yearsData ) {
                yearlyTotalsArray.push(yearsData[object]);
            }
            plotTheGraph(yearlyTotalsArray);
            returnV = true;
        } else {
            console.log("just one year dude:", yearsData);
        }
    }
    return returnV;
}

function plotTheGraph (yearlyTotals) {

    var margin = {top: 30, right: 20, bottom: 20, left: 50},
        width = 400 - margin.left - margin.right,
        height = 270 - margin.top - margin.bottom;

    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);

    var xAxis = d3.svg.axis().scale(x)
        .orient("bottom")
        .ticks(5);
//        .tickFormat(d3.time.format("%y"));

    //define grid lines functions
    function makeXAxis () {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(5);
    }

    function makeYAxis () {
        return d3.svg.axis()
            .scale(y)
            .ticks(5)
            .orient("left");
    }

    var yAxis = d3.svg.axis().scale(y)
        .orient("left").ticks(5)
        .tickFormat(d3.format("d")); //show integers only


//    if we want to fill the area, we need to tell it what space to fill
    var area = d3.svg.area()
        .x(function (d) {
            return x(d.date);
        })
        .y0(height)
        .y1(function (d) {
            return y(d.victims);
        });

    var valueline = d3.svg.line()
        .interpolate("linear")
        .x(function (d) {
            return x(d.date);
        })
        .y(function (d) {
            return y(d.victims);
        });


    function getXCoord (d) {
        return x(d.date);
    }

    function getYCoord (d) {
        return y(d.victims);
    }

    //add the graph to the sidebar
//    var svg = d3.select("#graph-container")
    var svg = d3.select("#sidebar")
        .insert("svg", ':first-child')
        .attr("id", "graph")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


//    Scale the range of the data
    x.domain(d3.extent(yearlyTotals, function (d) {
        return d.date;
    }));
    y.domain([0, d3.max(yearlyTotals, function (d) {
        return d.victims;
    })]);


//    fill the area
//    svg.append("path")
//        .datum(data)
//        .attr("class", "area")
//        .attr("d", area);

//    draw y-axis grid lines
    svg.append("g")
        .attr("class", "grid")
        .call(makeYAxis()
            .tickSize(-width, 0, 0)
            .tickFormat("")
        );

//    add the actual graph line
    svg.append("path")
        .attr("d", valueline(yearlyTotals));


//    add the x-axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("stroke", "white")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

//    label the x-axis
//    svg.append("text")
//        .attr("x", width/2 )
//        .attr("y", height + margin.bottom - 5)
//        .attr("class","axis-label")
//        .text("Έτος");

//    add the y-axis
    svg.append("g")
        .attr("class", "y axis")
        .attr("stroke", "white")
        .call(yAxis);

//    label the y-axis
//    svg.append("text")
//        .attr("transform", "rotate(-90)")
//        .attr("y", 0 - margin.left)
//        .attr("x",0 - (height / 2))
//        .attr("class","axis-label")
//        .attr("dy", "1em")
//        .text("Θύματα");

//    draw small circles at each data point
    svg.selectAll("circle")
        .data(yearlyTotals)
        .enter().append("circle")
        .attr("class", "graph-dot")
        .attr("r", 5)
        .attr("cx", getXCoord)
        .attr("cy", getYCoord);


//    The tickSize function can only accept a number as argument, not a function.
//    After the axis is drawn, select all the tick lines and resize them according to their data value.
//    Just remember that you'll have to do this after every axis redraw, as well.

    d3.selectAll("g.y.axis g.tick line")
        .attr("x2", function (d) {
            //d for the tick line is the value
            //of that tick
            //(a number between 0 and 1, in this case)
            if ( d === parseInt(d) ) { //if it's an even multiple of 10%
                return -5;
            }
            else {
                return 0;
            }
        });
}


//a helper function to get the correct data object depending on the data structure
//that is passed to the drawMarker function. It could be an array of arrays of objects (for normal mode)
//or it could be an array of objects (for timelapse mode)

function getNodeDataFromDataset (dataset) {
    var returnV;
    if ( dataset.length > 0 ) {
        returnV = dataset[0];
    } else {
        returnV = dataset;
    }

    return returnV;
}

/*
 param data: the acutal array of data that we use to draw the markers. An array of arrays. Each array
 param flat_data: a flattened version of the the data w
 */
function drawMarkers (data, flatData, max) {

//    create a linear scale for the radius based on the total number of victims. This will be used to
//    scale the marker radius depending on the number of victims
    var scaleRadius = d3.scale.linear().domain([0, max]).range([10, 30]);

    /* Initialize the SVG layer */
    map._initPathRoot();

    /* We simply pick up the SVG from the map object */
    var svg = d3.select("#map").select("svg"),
        g = svg.append("g");

    //draw the markers
    //notice that we use d[0] since our array contains arrays of objects because of the aggregate feature we implement
    var circles = g.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", function (d) {
            return getXCoordinate(d);
        })
        .attr("cy", function (d) {
            return getYCoordinate(d);
        })
        .attr("fill", function (d) {
            return getColor(d);
        })
        .attr("r", function (d) {
            return getRadius(d);
        })
        .attr("class", "marker")
        .attr("opacity", function (d) {
            return getDefaultOpacity(d);
        })
        .attr("fill-opacity", function (d) {
            return getDefaultFillOpacity(d);
        })
        .attr("stroke-width", function (d) {
            return getDefaultStrokeWidth(d);
        });
//
    //in the dataset there was an object with latlng Nan, so here we check for the validity of the data
    //before showing the marker
    function getDefaultOpacity (d) {
        var returnV = 1;
        var object = getNodeDataFromDataset(d);
        if ( object.latlng === null ) {
            returnV = 0;
        }
        return returnV;
    }

    function getDefaultStrokeWidth (d) {
        if ( mode === MODE.Normal ) {
            return 0;
        } else {
            return 1;
        }
    }

    function getDefaultFillOpacity (d) {
        if ( mode === MODE.Normal ) {
            return 0;
        } else {
            return 0.6;
        }
    }

    function getColor (d) {
        return "#FF9900";
//        var obj = getNodeDataFromDataset(d);
//        if (d.length > 1)
//            return markerColor["Aggregate"]
//        else
//            return markerColor[obj['data']['Cause_of_death']]  || "#22AA44";
    }

    function getRadius (d) {
        var radius = 0;
        if ( mode === MODE.Timelapse && !isTimelapseFinished ) {
            return 0; //default when in Timelapse mode. Then we'll increase the radius as new events get added
        } else {
            if ( d.total === undefined ) {
                radius = 10;
            } else {
                radius = scaleRadius(d.total);
            }
        }
        return radius;
    }

    function getXCoordinate (d) {
        var returnV;
        var data = getNodeDataFromDataset(d);
        if ( data.latlng != null ) {
            returnV = map.latLngToLayerPoint(data.latlng).x;
        }
        return returnV;
    }

    function getYCoordinate (d) {
        var returnV;
        var data = getNodeDataFromDataset(d);
        if ( data.latlng != null ) {
            returnV = map.latLngToLayerPoint(data.latlng).y;
        }
        return returnV;
    }


    //compute the delay based on each node's inde so they animate sequentially
    var delay = function (d, i) {
        return i * timelapseAnimationDuration;
    };

//    show the markers with on the map with an animation
    if ( mode === MODE.Normal ) {
        console.log("normal mode");
        circles.transition()
            .duration(1000)
            .style("fill-opacity", 0.6)
            .style("stroke-width", 1)
            .each("end", function (d, i) {
                //when all transitions have finished attach the interaction triggers
                if ( i === data.length - 1 ) {
                    attachInteractionTriggers();
                    showTimelapseButton();
                }
            });

//        else start the timelapse chained transitions
    } else if ( mode === MODE.Timelapse ) {

        console.log("timelapse mode");
        //select all the animated circles (which don't exist yet) and attach to them the data and the chained transitions
        g.selectAll('animated-circle')
            .data(flatData)
            .enter() //add them to the map
            .append('circle') //create a new set of circle elements
            .attr("cx", function (d) {
                return getXCoordinate(d);
            })
            .attr("cy", function (d) {
                return getYCoordinate(d);
            })
            .attr("r", 0) //making them invisible
            .style("fill-opacity", function (d) {
                return getDefaultFillOpacity(d);
            })
            .style("stroke-width", function (d) {
                return getDefaultStrokeWidth(d);
            })
            .style("opacity", function (d) {
                return getDefaultOpacity(d);
            })
            .style("fill", "#FF9900")
            .attr("class", "animated-circle")
            .transition() //animate them in, increasing the radius and the opacity
            .duration(timelapseAnimationDuration)
            .delay(delay)
            .style("fill-opacity", 0.9)
            .style("stroke-width", 1)
            .attr("r", 80)
            .transition() //and then animate them back to nothingness
            .duration(timelapseAnimationDuration)
            .style("fill-opacity", 0.0)
            .attr("r", 0) //setting the radius to 0 will make them invisible
            .remove() //remove the animated marker
            .each('start', started) //do something on each transition start
            .each('end', ended); //do something on each transition end
    }

    function started (d) {
//        var parsedDate = d3.time.format("%Y-%m-%dT%H:%M:%SZ").parse;
//        var formatDate = d3.time.format("%d-%m-%Y");
//        var parsedAndFormatted = formatDate(parsedDate(d.data.Date));
//
        var content = d.data.Location + ", " + getFormattedDate(d.data.Date);
        setTimelapseButtonText(content);
    }


    //resize the circle based on the recently animated event
    function ended (d, i) {
        //get the animated circle's x/y coordinates
        var thisCx = d3.select(this).attr("cx");
        var thisCy = d3.select(this).attr("cy");

        //d3 filtering
        //select the .marker object (the circles drawn in the beginning) that matches our animated circles x,y coordinates
        var node = d3.selectAll(".marker").filter(function () {
            var cx = d3.select(this).attr("cx");
            var cy = d3.select(this).attr("cy");
            return (thisCx === cx && thisCy === cy);
        });

        //get the data we want to use
        var event_victims = d.data.Dead_and_missing; //get the number of victims for this event

        //get the data that is associated with the existing circle marker on the map
        var data = node.data()[0];

        //we create a new data property that we will keep incrementing and use for the resizing of our marker
        if ( data.newtotal === undefined )
            data.newtotal = event_victims;
        else
            data.newtotal += event_victims;

        //resize the marker on the spot depending on the new total
        node.transition()
            .duration(800)
            .attr("r", scaleRadius(data.newtotal));

//        when the timelapse animation has finished...
        if ( i === flatData.length - 1 ) {
            isTimelapseFinished = true;
            attachInteractionTriggers();
            mode = MODE.Normal;
            resetCanvasFromTimelapse(false);

        }
    }

    function update () {
        circles.attr("cx", function (d) {
            return getXCoordinate(getNodeDataFromDataset(d));
        });
        circles.attr("cy", function (d) {
            return getYCoordinate(getNodeDataFromDataset(d));
        });
        circles.attr("r", function (d) {
            return getRadius(d);
        });
    }

    map.on("viewreset", update);
    update();


}


//get the largest value from the 'Dead_and_missing' field. This is
//necessary so we can use it as our maximum scale value
function getMaxVictims (data) {
    var returnV = maxDeadAndMissing;
    //compute it once and then just return it
    if ( maxDeadAndMissing === 0 ) {
        returnV = d3.max(data, function (d) {
            return d.Dead_and_missing;
        });
    }
    return returnV;
}

function loadData () {
    d3.json("data/themigrantsfiles-gr.json", function (error, json) {
        console.log("data", json);
        incomingData = json;

//        //first process the incoming data
//        processData(incomingData);
        incomingData.forEach(parseData);

        maxDeadAndMissing = getMaxVictims(incomingData);

        //then parse the json objects to an array
        dataArray = getDataAsArrayOfArrays(aggregatedEvents);


        //sort the array in descending order depending on their total.
        //since the total determines the radius of the circles the smaller
        //circles will be drawn last and so will be in front of the larger ones
        dataArray.sort(function (a, b) {
            return (b.total - a.total);
        });

        loadNormal();

//        var flatArray = []
        var array = getDataAsArrayOfArrays();
        for ( var index in array ) {
            var objects = array[index]; //this is always an array of objects with length >=1
            for ( var i = 0; i < objects.length; i++ ) {
                flatArray.push(objects[i]);
            }
        }

        //sort the array in ascending order based on the date object
        flatArray.sort(function (a, b) {
            return ( new Date(a.data.Date) - new Date(b.data.Date));
        });

        //when the timelapse button is clicked
        $("#button-wrapper").click(function () {
            toggleTimelapse();
        });

        console.log("flat:", flatArray.length);

    });
}

function cleanMapAndRedraw () {
    console.log("cleanup");
    map.panTo(athens);

    //if this methods gets called while the timelapse mode is playing (so interrupting it), if I don't transition their removal, the timelapse transition keeps playing

    //there must be a better more correct way of doing this, but basically I need to reset the newtotal to 0 here
    //otherwise cumulative timelapse modes will result in bigger circles
    d3.selectAll(".marker")
        .each(function (d) {
            d.newtotal = 0;
        });


    d3.selectAll('.animated-circle')
        .transition()
        .attr("r", 0)
        .remove();

    d3.selectAll('circle')
        .transition() //start transition here so it cancels other running transitions
        .duration(timelapseAnimationDuration)
        .attr("r", 0)
        .each('end', function (d, i) {
            if ( i === dataArray.length - 1 ) {
                d3.selectAll('g')
                    .remove();
                if ( mode === MODE.Normal ) {
                    loadNormal();
                } else {
                    startTimelapse();
                }
            }
        })
        .remove()
}

var timelapseAnimationDuration = 400;

function loadNormal () {
    mode = MODE.Normal;
    //finally draw the markers for the given array
    drawMarkers(dataArray, null, maxDeadAndMissing);

}

function toggleTimelapse () {

    if ( mode === MODE.Timelapse ) {
        mode = MODE.Normal;
        isTimelapseFinished = false;
    }
    else {
        mode = MODE.Timelapse;
    }

    resetCanvasFromTimelapse(true);
}

function getFormattedDate (date) {
    var parsedDate = d3.time.format("%Y-%m-%dT%H:%M:%SZ").parse;
    var formatDate = d3.time.format("%d-%m-%Y");
    return formatDate(parsedDate(date));
}


function resetCanvasFromTimelapse (cleanMap) {

    d3.select("#button-wrapper")
        .transition()
        .duration(300)
        .style('width', function () {
            if ( mode === MODE.Timelapse ) {
                return '400px';
            }
            else
                return '170px';
        })
        .each('end', function () {
            resetTimelapseButtonText();

            if ( cleanMap ) {
                cleanMapAndRedraw();
            }
        });
}

function startTimelapse () {
    console.log("start timelapse");
    drawMarkers(dataArray, flatArray, maxDeadAndMissing);
}

function hideTimelapseButton () {
    d3.select('#button-wrapper').transition(400).style('opacity', 0);
}

function showTimelapseButton () {
    d3.select('#button-wrapper').transition(400).style('opacity', 1); //and show the timelapse play button
}

function resetTimelapseButtonText () {
    console.log("reset xronogrammi");
    d3.select("#btn-timelapse")
        .attr("value", "Χρονοεγγραφή");
}

function setTimelapseButtonText (content) {
    d3.select('#btn-timelapse').attr('value', content);
}